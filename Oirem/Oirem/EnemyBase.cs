﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Oirem
{
    abstract class EnemyBase
    {
        //Base attributes
        int enemyHealth;
        int movementSpeed;
        Rectangle enemyRectangle;
        Texture2D enemySprite;

        public void Movement(int windowsWidth, int windowsHeight)
        {
            if (enemyRectangle.X != windowsWidth)
            {
                enemyRectangle.X += movementSpeed;
            }
        }

        public EnemyBase(int x, int y, int width, int height, int speed)
        {
            enemyRectangle.X = x;
            enemyRectangle.Y = y;
            enemyRectangle.Width = width;
            enemyRectangle.Height = height;
            movementSpeed = speed;
        }

        //Properties for base attributes
        public int EnemyHealth
        {
            get
            {
                return enemyHealth;
            }
        }
        public int MovementSpeed
        {
            get
            {
                return movementSpeed;
            }
        }
        public int PosX
        {
            get
            {
                return enemyRectangle.X;
            }
            set
            {
                enemyRectangle.X = value;
            }
        }
        public int PosY
        {
            get
            {
                return enemyRectangle.Y;
            }
            set
            {
                enemyRectangle.Y = value;
            }
        }
    }
}
