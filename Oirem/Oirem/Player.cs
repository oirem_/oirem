﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Oirem
{
    public class Player
    {
        //Base attributes
        int playerHealth;
        int playerDamage;
        string playerName;
        Rectangle playerRectangle;
        Texture2D playerSprite;


        //default constructor
        public Player()
        {

        }

        //Constructor
        public Player(int x, int y, int width, int height, string name, Texture2D sprite, int health, int damage)
        {
            playerRectangle.X = x;
            playerRectangle.Y = y;
            playerRectangle.Width = width;
            playerRectangle.Height = height;
            playerName = name;
            playerSprite = sprite;
            playerHealth = health;
            playerDamage = damage;
            
        }

        //Properties for base attributes
        public int PlayerHealth
        {
            get
            {
                return playerHealth;
            }
            set
            {
                playerHealth = value;
            }
        }
        public string PlayerName
        {
            get
            {
                return playerName;
            }
        }
        public int PosX
        {
            get
            {
                return playerRectangle.X;
            }
            set
            {
                playerRectangle.X = value;
            }
        }
        public int PosY
        {
            get
            {
                return playerRectangle.Y;
            }
            set
            {
                playerRectangle.Y = value;
            }
        }
        public int PlayerDamage
        {
            get
            {
                return playerDamage;
            }
            set
            {
                playerDamage = value;
            }
        }

        // Method that moves the player by keyboard input
        public void Movement(Player player)
        {
            KeyboardState kbState = Keyboard.GetState();
            
            // Jumping
            if (kbState.IsKeyDown(Keys.W))
            {
                player.PosY -= 10;
            }
            // Left
            else if (kbState.IsKeyDown(Keys.A))
            {
                player.PosX--;
            }
            // Right
            else if (kbState.IsKeyDown(Keys.D))
            {
                player.PosX++;
            }
        }
    }
}
