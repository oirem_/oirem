﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Oirem
{
    public class Gravity
    {

        //method to apply gravity to both the player and the enemy by passing in their rectangles
        public void FallingGravity(Rectangle r1, Rectangle ground)
        { 

            if ((r1.Y + r1.Height) != ground.Y)
            {
                r1.Y++;
            }

        }

    }
}
