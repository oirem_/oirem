﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Oirem
{
    public class Map_Layout : Map_Collison
    {

        //attributes to hold loaded in content
        Texture2D background;
        Texture2D obstacle;
        Texture2D platform;
        Texture2D ground;

        //default constuctor
        public Map_Layout()
        {

        }

        //constructor to make map
        public Map_Layout(Texture2D back, Texture2D obst, Texture2D plat, Texture2D gro)
        {
            background = back;
            obstacle = obst;
            platform = plat;
            ground = gro;
        }

    }
}
