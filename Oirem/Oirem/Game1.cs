﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Oirem
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        //attributes for map
        Texture2D background;
        Texture2D ground;
        Texture2D platform;
        Texture2D obstacle;

        Map_Layout m1;
        Player p1;

        // Enum for gamestates
        enum Gamestates {MainMenu, PauseMenu, OptionsMenu, GameScreen};
        Gamestates gamestate;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            //creating a map object with attributes being passed in
            m1 = new Map_Layout();
            p1 = new Player();

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            m1.FallingGravity(p1.playerRectangle, m1.);

<<<<<<< HEAD
            
=======
            // If statements that will run depending on the gamestate the player is on
            if(gamestate == Gamestates.MainMenu)
            {

            }
            else if(gamestate == Gamestates.PauseMenu)
            {

            }
            else if (gamestate == Gamestates.OptionsMenu)
            {

            }
            else if(gamestate == Gamestates.GameScreen) // The game itself
            {

            }
>>>>>>> e204026ab681469c24a4799885ad106c80b40eb5

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            spriteBatch.Begin();

            // If statements that will draw depending on the gamestate
            if (gamestate == Gamestates.MainMenu)
            {

            }
            else if (gamestate == Gamestates.PauseMenu)
            {

            }
            else if (gamestate == Gamestates.OptionsMenu)
            {

            }
            else if(gamestate == Gamestates.GameScreen) // The game itself
            {

            }

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
