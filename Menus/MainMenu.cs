﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Threading.Tasks;

namespace Oirem_GUI
{
    class MainMenu
    {
        enum Choices
        {
            Options,
            Play,
            Quit
        }

        SpriteBatch SpriteBatch;
        Texture2D playButton;
        Texture2D optionsButton;
        Texture2D quitButton;
        Point size = new Point(150, 50);
        Point location = new Point(150, 50);
        

        public void Draw()
        {
            SpriteBatch.Begin();
            SpriteBatch.Draw(playButton, new Rectangle(location, size), Color.White);
            SpriteBatch.End();
        }
    }
}
